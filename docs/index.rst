.. SecKit_IDM documentation master file, created by
   sphinx-quickstart on Tue Oct  2 07:28:23 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to SecKit_IDM's documentation!
======================================

Welcome to SecKit (Success Enablement Content), the IDM add on family is designed to help the security user develop high quality enrichment for Enterprise Security's Asset and Identity Framework.

What apps are in the family today? Get started today by implementing in the following order

- :doc:`SecKit SA IDM Common <saidmcommon:index>`  - This add-on provides the base functionality (library) and defines network assets at the cidr block level. Usings this add on you can gain enrichment for location data (lat/lon) as well as categorize networks by function and compliance control.
- :doc:`SecKit TA IDM Windows <taidmwindows:index>` This add on collect network configuration data for enrichment of assets in ES
- :doc:`SecKit SA IDM Windows <saidmwindows:index>` This add on discovers assets and identity data using Splunk TA for Windows.


.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
